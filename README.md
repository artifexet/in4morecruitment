# In4moRecruitment

By default, I would use the [Material Design](https://material.angular.io) library. However, the basic components from it would be enough to solve this task. So I decided do not use material design.

If you are interested in material design solution, follow the link from the documentation.

* [Example](https://stackblitz.com/angular/jrgrgdvaqgj?file=app%2Ftable-filtering-example.ts)

## Used Tools

I chose the following tools to solve this task:

* Angular 7 - as main framework, because I work with angular every day,
* NGRX - for state management library, because in the task there was information about using redux
* Karama - for test runner, I know that `Jest` is preferred but karma is similar and default in angular stack
* ts-mockito - for mock engine in unit tests

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.5.

If you have any questions please feel free to contact me.

## Quick Start

1. Run `npm install` for download dependencies.
2. Run `npm run start` for run developer server and compile app
3. Run `npm run test` for run unit tests.

## Contact
Szymon Rybka<br>
artifex@gmail.com<br>
tel: 661535250

## Below automatically generated readme form angular cli

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
