import {ProductsListState} from '../products-list/store/products-list.state';

export interface RootState {
  productsList: ProductsListState;
}
