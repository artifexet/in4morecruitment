import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';

describe('AppComponent', () => {
  let fixture;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
  }));

  describe('when initialize', () => {
    it('should render ProductsListComponent', () => {
      // when
      fixture.detectChanges();

      // then
      const compiled = fixture.debugElement.nativeElement;
      expect(compiled.querySelector('app-products-list')).toBeTruthy();
    });
  });
});
