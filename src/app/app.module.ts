import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {HttpClientModule} from '@angular/common/http';
import {ProductsListComponent} from './products-list/components/products-list.component';
import {ProductsListSearchComponent} from './products-list/components/products-list-search/products-list-search.component';
import {ProductListTableComponent} from './products-list/components/product-list-table/product-list-table.component';
import {productListReducer} from './products-list/store/products-list.reducer';
import {ProductsService} from './products-list/services/products-service';

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    ProductsListSearchComponent,
    ProductListTableComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot({productsList: productListReducer}),
    StoreDevtoolsModule.instrument({}),
  ],
  providers: [ProductsService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
