import {defaultState, productListReducer} from './products-list.reducer';
import {ProductsListState} from './products-list.state';
import * as deepFreeze from 'deep-freeze';
import {ProductsListChangeSearchParamAction, ProductsListSetProductsAction} from './products-list.actions';
import {Product} from '../interfaces/product.interface';

describe('productListReducer', () => {
  describe('for undefined state', () => {
    it('should return the default state', () => {
      const action = {} as any;

      const result = productListReducer(undefined, action);
      expect(result).toEqual(defaultState);
    });
  });

  describe('for undefined action', () => {
    it('should return the unmodified state', () => {
      // given
      const action = {} as any;
      const state: ProductsListState = {
        searchParam: null,
        products: null,
      };

      // when
      deepFreeze(state);
      const result = productListReducer(state, action);

      // then
      expect(result).toEqual(state);
    });
  });

  describe('for set products action', () => {
    it('should return state with new products', () => {
      // given
      const state: ProductsListState = {
        searchParam: 'test search param',
        products: null,
      };
      const products: Product[] = [{name: 'testName', description: 'testDescription'}]
      const action = new ProductsListSetProductsAction(products);

      // when
      deepFreeze(state);
      const result = productListReducer(state, action);

      // then
      expect(result.products).toEqual(products);
      expect(result.searchParam).toEqual(state.searchParam);
    });
  });

  describe('for set search param action', () => {
    it('should return state with new search param', () => {
      // given
      const state: ProductsListState = {
        searchParam: 'test search param',
        products: [{name: 'testName', description: 'testDescription'}],
      };
      const newSearchParam: string = 'hello world';
      const action = new ProductsListChangeSearchParamAction(newSearchParam);

      // when
      deepFreeze(state);
      const result = productListReducer(state, action);

      // then
      expect(result.products).toEqual(state.products);
      expect(result.searchParam).toEqual(newSearchParam);
    });
  });
});
