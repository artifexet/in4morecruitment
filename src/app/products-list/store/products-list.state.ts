import {Product} from '../interfaces/product.interface';

export interface ProductsListState {
  products: Product[];
  searchParam: string;
}
