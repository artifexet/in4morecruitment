import {Action} from '@ngrx/store';
import {Product} from '../interfaces/product.interface';

export const PRODUCT_LIST_CHANGE_SEARCH_PARAM = '[ProductsList] PRODUCT_LIST_CHANGE_SEARCH_PARAM';
export const PRODUCT_LIST_SET_PRODUCTS = '[ProductsList] PRODUCT_LIST_SET_PRODUCTS';

export class ProductsListChangeSearchParamAction implements Action {
  readonly type = PRODUCT_LIST_CHANGE_SEARCH_PARAM;

  constructor(public searchParam: string) {}
}

export class ProductsListSetProductsAction implements Action {
  readonly type = PRODUCT_LIST_SET_PRODUCTS;

  constructor(public products: Product[]) {}
}

export type ProductListsAction = ProductsListChangeSearchParamAction | ProductsListSetProductsAction;
