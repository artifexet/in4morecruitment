import {getFilteredProducts} from './products-list.selectors';

describe('getFilteredProducts', () => {
  const firstSampleProduct = {
    'name': 'firstSampleProduct name',
    'description': 'thirdSampleProduct description'
  };
  const secondSampleProduct = {
    'name': 'secondSampleProduct name',
    'description': 'secondSampleProduct description'
  };
  const thirdSampleProduct = {
    'name': 'thirdSampleProduct name',
    'description': 'thirdSampleProduct description'
  };
  const sampleProducts = [
    firstSampleProduct,
    secondSampleProduct,
    thirdSampleProduct,
  ];

  describe('for empty search param', () => {
    it('should return all products', () => {
      // given
      const searchParam = undefined;

      // when
      const result = getFilteredProducts.projector(sampleProducts, searchParam);

      // then
      expect(result).toEqual(sampleProducts);
    });
  });

  describe('for valid search param', () => {
    it('should return all products that name contains this param', () => {
      // given
      const expectedNumberOfResults = 1;
      const searchParam = 'secondSampleProduct';

      // when
      const result = getFilteredProducts.projector(sampleProducts, searchParam);

      // then
      expect(result).toContain(secondSampleProduct);
      expect(result.length).toEqual(expectedNumberOfResults);
    });

    it('should return all products that description contains this param', () => {
      // given
      const expectedNumberOfResults = 3;
      const searchParam = 'description';

      // when
      const result = getFilteredProducts.projector(sampleProducts, searchParam);

      // then
      expect(result).toContain(firstSampleProduct);
      expect(result).toContain(secondSampleProduct);
      expect(result).toContain(thirdSampleProduct);
      expect(result.length).toEqual(expectedNumberOfResults);
    });

    it('should be camel case insensitive', () => {
      // given
      const expectedNumberOfResults = 1;
      const searchParam = firstSampleProduct.name.toUpperCase();

      // when
      const result = getFilteredProducts.projector(sampleProducts, searchParam);

      // then
      expect(result).toContain(firstSampleProduct);
      expect(result.length).toEqual(expectedNumberOfResults);
    });

    describe('when no result found', () => {
      it('should return empty array', () => {
        // given
        const expectedNumberOfResults = 0;
        const searchParam = 'no exist param';

        // when
        const result = getFilteredProducts.projector(sampleProducts, searchParam);

        // then
        expect(result.length).toEqual(expectedNumberOfResults);
      });
    });
  });
});
