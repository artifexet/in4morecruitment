import {PRODUCT_LIST_CHANGE_SEARCH_PARAM, PRODUCT_LIST_SET_PRODUCTS, ProductListsAction} from './products-list.actions';
import {ProductsListState} from './products-list.state';

export const defaultState: ProductsListState = {
  products: [],
  searchParam: '',
};

export function productListReducer(state = defaultState, action: ProductListsAction) {
  switch (action.type) {
    case PRODUCT_LIST_CHANGE_SEARCH_PARAM:
      return {
        ...state,
        searchParam: action.searchParam
      };
    case PRODUCT_LIST_SET_PRODUCTS:
      return {
        ...state,
        products: action.products
      };
    default:
      return state;
  }
}
