import {RootState} from '../../store/root-state';
import {createSelector} from '@ngrx/store';
import {Product} from '../interfaces/product.interface';

export const getProducts = (state: RootState) => {
  return state.productsList ? state.productsList.products : [];
};

export const getSearchParam = (state: RootState) => {
  return state.productsList ? state.productsList.searchParam : '';
};

export const getFilteredProducts = createSelector(
  getProducts,
  getSearchParam,
  (selectedProducts: Product[], selectedSearchParam: string) => {
    if (selectedSearchParam && selectedProducts) {
      return filterProductsByText(selectedProducts, selectedSearchParam);
    } else {
      return selectedProducts;
    }
  });

export const filterProductsByText = function (selectedProducts: Product[], selectedSearchParam: string): Product[] {
  return selectedProducts.filter((product: Product) => {
    return containsText(product.description, selectedSearchParam) ||
      containsText(product.name, selectedSearchParam);
  });
};

export const containsText = function (text: string, searchedText: string): boolean {
  return text.toLocaleLowerCase().indexOf(searchedText.toLocaleLowerCase()) > -1;
};

