import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {HttpClient} from '@angular/common/http';
import {ProductsListState} from '../store/products-list.state';
import {Product} from '../interfaces/product.interface';
import {ProductsListSetProductsAction} from '../store/products-list.actions';

@Injectable()
export class ProductsService {

  constructor(private store: Store<ProductsListState>,
              private http: HttpClient) {
  }

  fetchProducts(): void {
    this.http.get<Product[]>('assets/products.json').subscribe(products => {
      this.store.dispatch(new ProductsListSetProductsAction(products));
    });
  }
}
