import {TestBed} from '@angular/core/testing';
import {ProductsService} from './products-service';
import {HttpClient} from '@angular/common/http';
import {anything, deepEqual, instance, mock, verify, when} from 'ts-mockito';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs';
import {RootState} from '../../store/root-state';
import {ProductsListSetProductsAction} from '../store/products-list.actions';
import {Product} from '../interfaces/product.interface';

describe('ProductsService', () => {
  const sampleResponse = [];
  let mockedHttpClient: HttpClient;
  let mockedStore: Store<RootState>;
  let httpResponseSubject: Subject<Product[]>;
  let service: ProductsService;

  beforeEach(() => {
    mockedHttpClient = mock(HttpClient);
    mockedStore = mock(Store);
    httpResponseSubject = new Subject();

    TestBed.configureTestingModule({
      providers: [
        ProductsService,
        {provide: HttpClient, useValue: instance(mockedHttpClient)},
        {provide: Store, useValue: instance(mockedStore)},
      ]
    });

    when(mockedHttpClient.get(anything())).thenReturn(httpResponseSubject);
    service = TestBed.get(ProductsService);
  });


  describe('when called fetch()', () => {
    it('should fetch for products', () => {
      // when
      service.fetchProducts();

      // then
      verify(mockedHttpClient.get(anything())).once();
    });
  });

  describe('after fetch() complete', () => {
    it('should update store with new data', () => {
      // given
      service.fetchProducts();

      // when
      httpResponseSubject.next(sampleResponse);

      // then
      verify(mockedStore.dispatch(deepEqual(new ProductsListSetProductsAction(sampleResponse)))).once()
    });
  });
});
