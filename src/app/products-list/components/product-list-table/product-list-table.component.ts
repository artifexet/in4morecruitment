import {Component, OnInit} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {Observable} from 'rxjs';
import {RootState} from '../../../store/root-state';
import {getFilteredProducts} from '../../store/products-list.selectors';
import {Product} from '../../interfaces/product.interface';

@Component({
  selector: 'app-product-list-table',
  templateUrl: './product-list-table.component.html',
  styleUrls: ['./product-list-table.component.scss']
})
export class ProductListTableComponent implements OnInit {
  public productList$: Observable<Product[]>;

  constructor(private store: Store<RootState>) {
  }

  ngOnInit(): void {
    this.productList$ = this.store.pipe<Product[]>(select(getFilteredProducts));
  }
}
