import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {ProductListTableComponent} from './product-list-table.component';
import {anything, instance, mock, when} from 'ts-mockito';
import {Store} from '@ngrx/store';
import {Subject} from 'rxjs';
import {RootState} from '../../../store/root-state';
import {Product} from '../../interfaces/product.interface';

describe('ProductListTableComponent', () => {
  const sampleProducts: Product[] = [
    {
      description: 'firstTestDescription',
      name: 'firstTestName'
    },
    {
      description: 'secondTestDescription',
      name: 'secondTestName'
    }
  ];

  let mockedStore: Store<RootState>;
  let productsSubject: Subject<Product[]>;
  let component: ProductListTableComponent;
  let fixture: ComponentFixture<ProductListTableComponent>;

  beforeEach(async(() => {
    mockedStore = mock(Store);
    productsSubject = new Subject();

    TestBed.configureTestingModule({
      declarations: [ProductListTableComponent],
      providers: [
        {provide: Store, useValue: instance(mockedStore)},
      ]
    }).compileComponents();

    when(mockedStore.pipe(anything())).thenReturn(productsSubject);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('when initialize', () => {
    it('should render current filtered products list', () => {
      // when
      productsSubject.next(sampleProducts);
      fixture.detectChanges();
      // then
      testTableContent(fixture, sampleProducts);
    });
  });

  describe('after filtered products list change', () => {
    it('should display new products list', () => {
      // given
      const newProductList: Product[] = [
        {
          description: 'new product description',
          name: 'new product name'
        }
      ];
      productsSubject.next(sampleProducts);
      fixture.detectChanges();

      // when
      productsSubject.next(newProductList);
      fixture.detectChanges();

      //then
      testTableContent(fixture, newProductList);
    });
  });
});

function testTableContent(fixture: ComponentFixture<ProductListTableComponent>,
                          productsThatShouldBeRendered: Product[]) {
  const tableHeaderRowCount = 1;
  const expectedCellCount = 2;
  const expectedNumberOfRows = productsThatShouldBeRendered.length + tableHeaderRowCount;
  const compiled = fixture.debugElement.nativeElement;
  const tableRows = compiled.querySelectorAll('tr');


  //then
  expect(tableRows.length).toEqual(expectedNumberOfRows);
  productsThatShouldBeRendered.forEach((product, index) => {
    const cells = tableRows[index + 1].querySelectorAll('td');
    expect(cells.length).toEqual(expectedCellCount);
    expect(cells[0].textContent).toEqual(product.name);
    expect(cells[1].textContent).toEqual(product.description);
  });
}
