import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductsListSearchComponent} from './products-list-search.component';
import {Store} from '@ngrx/store';
import {deepEqual, instance, mock, verify} from 'ts-mockito';
import {RootState} from '../../../store/root-state';
import {ProductsListChangeSearchParamAction} from '../../store/products-list.actions';

describe('ProductsListSearchComponent', () => {
  let mockedStore: Store<RootState>;
  let component: ProductsListSearchComponent;
  let fixture: ComponentFixture<ProductsListSearchComponent>;

  beforeEach(async(() => {
    mockedStore = mock(Store);

    TestBed.configureTestingModule({
      declarations: [ProductsListSearchComponent],
      providers: [
        {provide: Store, useValue: instance(mockedStore)},
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsListSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  describe('when user write something in input text', () => {
    it('should update store with new data', () => {
      // given
      const sampleText: string = 'Hello World';
      const compiled = fixture.debugElement.nativeElement;
      const inputElement = compiled.querySelector('input');

      // when
      inputElement.value = sampleText;
      inputElement.dispatchEvent(new Event('input'));
      fixture.detectChanges();

      // then
      verify(mockedStore.dispatch(deepEqual(new ProductsListChangeSearchParamAction(sampleText)))).once();
    });
  });
});
