import {Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {ProductsListState} from '../../store/products-list.state';
import {ProductsListChangeSearchParamAction} from '../../store/products-list.actions';

@Component({
  selector: 'app-products-list-search',
  templateUrl: './products-list-search.component.html',
  styleUrls: ['./products-list-search.component.scss']
})
export class ProductsListSearchComponent {

  constructor(private store: Store<ProductsListState
    >) {
  }

  changeSearchParam(text: string): void {
    this.store.dispatch(new ProductsListChangeSearchParamAction(text));
  }

}
