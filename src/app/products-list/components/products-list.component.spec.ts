import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {ProductsListComponent} from './products-list.component';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {instance, mock, verify} from 'ts-mockito';
import {ProductsService} from '../services/products-service';

describe('ProductListComponent', () => {
  let mockedProductsService: ProductsService;
  let component: ProductsListComponent;
  let fixture: ComponentFixture<ProductsListComponent>;

  beforeEach(async(() => {
    mockedProductsService = mock(ProductsService);

    TestBed.configureTestingModule({
      declarations: [ProductsListComponent],
      providers: [
        {provide: ProductsService, useValue: instance(mockedProductsService)},
      ],
      schemas: [NO_ERRORS_SCHEMA]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsListComponent);
    component = fixture.componentInstance;
  });

  describe('when initialize', () => {
    it('should fetch for new products', () => {
      // when
      fixture.detectChanges();

      // then
      verify(mockedProductsService.fetchProducts()).once();
    });
  });
});
